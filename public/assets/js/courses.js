//capture the html body which will display the content coming from the database

console.log("hihihi")
let modalButton = document.querySelector('#adminButton')
let container = document.querySelector('#coursesContainer')
//we are going to take the value of the isAdmin property from the local storage.
let isAdmin = localStorage.getItem("isAdmin");
let cardFooter;


if(isAdmin === "false" || !isAdmin){

	modalButton.innerHTML = null

	//if a user is a regular user do not show the add course button
}else{
	modalButton.innerHTML = `
	<div class="col-md-2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a></div>
	`
}

fetch('https://evening-island-69458.herokuapp.com/api/courses/').then(
	res => res.json()).then(data => {

	console.log(data);
	//declare a variable that will display a result in the browser depending on the return
	let courseData;
	//create a control structure that will determine the value that the variable will hold.
	if(data.length < 1) {
		courseData = "No Course Available"
	}else{
		//we will iterate the courses collection and display each course inside the browser
		courseData = data.map(course => {
			//lets check the make up of each element 
			console.log(course._id);
			//if the user is a regular user, display the enroll button and display course button.

			if(isAdmin == "false" || !isAdmin)
			{
				cardFooter = `
				<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View Course details</a>

				
				`
			}else{
				cardFooter = `
				<a href="./editCourse.html?courseId=${course._id}" class="btn btn-default text-black btn-block">Edit</a>
				<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-default text-black btn-block">Disable Course</a>
				`
			}

			return(
				`
	<div class="col-md-6 my-3">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title"> ${course.name}</h5>
				 ${course.description}
				<p class="card-text text-left"></p>
				 ${course.price}
				<p class="card-text text-left"></p>
				 ${course.createdOn}
			</div>
			<div class="card-footer"></div>
				${cardFooter}
		</div>
	</div>
				`//we attached a query string in the url which allows us to embed the ID from the database record into the query string.
				// the ? inside the url "acts" as a "separator" it indicates the end of a url resource path, and indicates the start of the "querty string".
				//# -> this was originally "used" to jump to an specific element with the same id name/value.
				)
		}).join("")
		//we used the join() to create a return of a new string
		//it contatenated all the objects inside the array and converted each to a string data type
	}

	container.innerHTML = courseData;
})