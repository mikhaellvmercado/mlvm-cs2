console.log("hi")

//the first thing that we need to fo is to identify 
//which course it needs to display in the browser

//we are going to use the target course id to identify the correct course properly

let params = new URLSearchParams(window.location.search)
//window.location -> returns a location object with information about the "current" location
//of the document
//.search => contains the query string section of the current url
//query string is an object

//search property returns an object of type stringString.
//URLSearchParams() => this method/constructor creates and returns a URLSearchParams object.
//"URLSearchParams" -> this describes the interface that defines utility methods to work with the querty string of a url.
//new -> instantiate a user-defined object.
//yes it is a class -> template for creating the object.

//params = {
//	"courseId": "....id ng course that we passed"
//}
let id = params.get('courseId');
console.log(id)

let token = localStorage.getItem('token')
console.log(token)

//LETS CAPTURE THE SECTIONS OF THE HTML BODY
let name = document.querySelector("#courseName")
let desc = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")
let enroll = document.querySelector("#enrollmentContainer")

fetch(`https://evening-island-69458.herokuapp.com/api/courses/${id}`).then(
	res => res.json()).then(data =>{
	console.log(data)
	name.innerHTML = data.name
	desc.innerHTML = data.description
	price.innerHTML = data.price
	enroll.innerHTML = `<a href="#" class="btn btn-success text-white btn-block">Enroll</a>`
	// `<a href="#" class="btn btn-success text-white btn-block">Enroll</a>`
})//if the result is displayed in the console it means you were able to properly pass the courseid and successfully created your fetch request



